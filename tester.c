#include "lib/pathfinder.h"
#include <stdio.h>

int main(int argc, char** argv){
	pathfDebugMode(false);
	printf("pathfinder tester v1.0\n");
	FILE* fl;
	fl=fopen(argv[1],"r");
	if(fl!=NULL){
		unsigned long start, end, r;
		printf("SYSTEM: reading file %s...\n", argv[1]);
		fscanf(fl,"%lu %lu %lu",&start, &end, &r);
		printf("SYSTEM: file input obtained: start node ID is %lu, end node ID is %lu, network is %lu node%s big\n",start, end, r,(r>1)?"s":"");

		Network net=initNetwork();
		setNetwork(&net, r);

		for(unsigned long i = 0; i < r; i++){
			unsigned long id,size;
			int x, y;
			
			printf("SYSTEM: reading file for node %lu\n",i);
			fscanf(fl, "%lu %d %d %lu", &id, &x, &y, &size);
			printf("SYSTEM: file input obtained:ID=%lu, x=%d, y=%d; node is adjacent to %lu node%s\n", id, x, y, size, (size>1)?"s":"");
			Node node=initNode();
			setNode(&node, id, x, y, size);
			printf("SYSTEM: adjacent nodes[");
			for(unsigned long j = 0; j < size; j++){
				unsigned long inp;
				fscanf(fl,"%lu",&inp);
				node.adjacent.array[j]=inp;
				printf("%lu%s",inp,(j<size-1)?", ":"]\n");
			}
			net.network[i]=node;
		}
		printf("SYSTEM: finished reading file, starting pathfinder...\n");
		Array path = findPath(start, end, net);
		printf("SYSTEM: pathfinder returned path of size %lu\n[", path.size);
		for(unsigned long i = 0; i < path.size; i++){
			printf("%lu%s",path.array[i],(i<path.size-1)?", ":"]\n");
		}
		return 0;

	}
	else{
		printf("WARNING: failed to open file. exiting...\n");
		return 1;
	}
}
