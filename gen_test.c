#include "lib/pathfinder.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	pathfDebugMode(true);

	unsigned long xdim=10;
	unsigned long ydim=10;
	char* unused;
	if(argc>1)
	{
		ydim=xdim=strtoul(argv[1], &unused, 10);
	}
	if(argc>2)
	{
		ydim=strtoul(argv[2], &unused, 10);
	}

	Network net=initNetwork();
	setNetwork(&net, xdim*ydim);

	for(unsigned long i = 0; i < xdim*ydim; ++i)
	{
		long xpos = i%xdim;
		long ypos = i/xdim;

		int adjsize=0;
		for(int x=-1; x<=1; ++x)
		{
			long nx=xpos+x;
			if(nx < 0 || nx >= xdim) continue;
			for(int y=-1; y<=1; ++y)
			{
				long ny=ypos+y;
				if(ny < 0 || ny >= ydim) continue;
				if(!x && !y) continue;
				adjsize++;
			}
		}

		Node node=initNode();
		setNode(&node, i, xpos, ypos, adjsize);
		int j=0;
		for(int x=-1; x<=1; ++x)
		{
			long nx=xpos+x;
			if(nx < 0 || nx >= xdim) continue;
			for(int y=-1; y<=1; ++y)
			{
				long ny=ypos+y;
				if(ny < 0 || ny >= ydim) continue;
				if(!x && !y) continue;
				node.adjacent.array[j++] = ny*xdim+nx;
			}
		}
		net.network[i]=node;
	}
	printf("SYSTEM: %lux%lu network generation complete\n",xdim,ydim);
#ifdef SKIP_PATHFINDER
	printf("skipping pathfinder...\n"); fflush(stdout);
#else
	printf("starting pathfinder...\n"); fflush(stdout);
	Array path = findPath(0, xdim*ydim-1, net);
	printf("SYSTEM: pathfinder returned path of size %lu\n[", path.size);
	for(unsigned long i = 0; i < path.size; i++){
		printf("%lu%s",path.array[i],(i<path.size-1)?", ":"]\n");
	}
	setArray(&path, 0);
#endif
	for(unsigned long i=0; i<net.size; ++i)
	{
		setNode(&net.network[i], -1, -1, -1, 0);
	}
	setNetwork(&net, 0);
	return 0;

}
