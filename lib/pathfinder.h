// cpathfinder is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// cpathfinder is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.

#ifndef _CPATHFINDER_H_
#define _CPATHFINDER_H_
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#define true 1
#define false 0

typedef struct Array{
	unsigned long size;
	unsigned long* array;
}Array;

typedef struct Node{
	unsigned long id;
	int x;
	int y;
	Array adjacent;
}Node;

typedef struct Tracker{
	bool visited;
	unsigned long node;
	float weight;
	unsigned long origin;
}Tracker;

typedef struct Network{
	unsigned long size;
	Node* network;
}Network;

typedef struct Map{
	unsigned long size;
	Tracker* map;
}Map;

void pathfDebugMode(bool state);
void printArray( Array out);
void printNode(Node node);
void printNetwork(Network net);

Network readGraphf(char* file);

void setArray(Array* array, unsigned long size);
Array initArray();

void setNetwork(Network* net, unsigned long size);
Network initNetwork();

void setNode(Node* node, unsigned long id, int x, int y, unsigned long size);
Node initNode();

Map setupMap(Network net);
Map initMap();

Tracker initTracker(Node node);
Tracker* findTracker(Map map, unsigned long node);
float getWeight(Tracker tracker, Tracker origin, Network net);

void appendArray(Array* input, unsigned long element);
void removeArray(Array* input, unsigned long element);
void searchRemove(Array* arr, unsigned long element);
void duplicateArray(Array reference, Array* target);
float distance(int x1, int y1, int x2, int y2);
Node* getNodeByIndex(Network* net, unsigned long nodeID);
unsigned long pathLength(Network nodes, Array path);

Array findPath(unsigned long startID, unsigned long endID, Network nodes);
#endif
