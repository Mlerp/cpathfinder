#include <stdio.h>
#include <stdlib.h>
#include "lib/pathfinder.h"

int main(int argc, char** argv){
	if(argc<=1){
		printf("usage: ./checkgraph [FILE1] [FILE2] [FILE3] ...");
	}
	else{
		for(int arg=1; arg<=argc; arg++){
			printf("SYSTEM: opening file %s");
			Network net=readgraphf(argv[arg]);
			if(net.size==0){
				printf("WARNING: failed to read file %s!", argv[arg]);
			}
			printf("SYSTEM: analyzing network in file %s\n",argv[arg]);
			for(long i=0; i < net.size; i++){//goes through every node
				Node A = net.network[i];
				for(long j=0; j < A.size; j++){//goes through all the adjacent nodes to current node
					bool asymm = true;
					Node B =getNodeByID(net, A.adjacent[j])
					for(long k = 0; k < B.size; k++){//goes through nodes present in target
						if(A.id == B.adjacent[k] && B.adjacent[k] != 0){
							asymm=false;
							B.adjacent[k]=0;
						}
					}
					printf("SYSTEM:egde %lu-%lu is %ssymmetrical\n",A.id,B.id, asymm?"a":"");
					A.adjacent[j]=0;
				}
			}
		}
	}
}
