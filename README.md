# dijkstra

a library including functions for taking advantage of Dijkstra's pathfinding algorithm on cospace open competition

-scripting API contents:
	
	Node; - a structure that contains information on a single node on the map
		unsigned long id; - an identification number of the node
		int x; - an X coordinate of the node
		int y; - an Y coordinate of the node
		unsigned long adjSize; - a variable storing the size of the adjacent array
		unsigned long* adjacent; - a pointer to the array listing adjacent nodes. it is expected for an array to list the IDs of the adjacent nodes
	
	Path; - a structure storing a given path of two points
		unsigned long pathSize; - a variable storing the size of the path
		unsigned long* path; - a pointer to the path. path is defined as an array of IDs of the nodes the current path consists of

	Node initNode(unsigned long id, int x, int y, unsigned long size); - a function that returns an initalized node
		unsigned long id; - an ID to be assigned to a new node
		int x; - an X coordinate of the node
		int y; - an Y coordinate of the node
		unsigned long size; - size of the array listing adjacent nodes
		
		initNode() returns a Node structure, meaning that it has to be assigned to the node as an variable to work
		usage example:
			#include "dijkstra.h"
			int main(void){
				struct Node foo = initNode(8571, 10, 15, 4);
			}
		code above will create a foo Node structure with an ID of 8571, X and Y coordinates of 10 and 15, and an array to store IDs of 4 adjacent nodes

	Path findPath(unsigned long startID, unsigned long endID, unsigned long nodesSize, struct Node* nodes); - a function that finds the shortest path between two given nodes
		unsigned long startID; - ID of the starting node
		unsigned long endID; - ID of the ending node
		unsigned long nodesSize; - amount of nodes in an given network
		struct Node* nodes; - a pointer to an array of nodes making up a given network
		-WARNING: as of 0.0.1, this function doesnt actually do anything
